FROM maven:3.6.3-jdk-11
VOLUME /tmp
COPY ./ ./
EXPOSE 8080
RUN mkdir -p /app/
RUN mkdir -p /app/logs/
RUN mvn -DDATASOURCE_URL="jdbc:postgresql://localhost:5432/cahaya" package -B
ADD target/cahaya-be-0.0.1-SNAPSHOT.jar /app/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "/app/app.jar"]