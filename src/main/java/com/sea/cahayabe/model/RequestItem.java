package com.sea.cahayabe.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity(name = "request_item")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class RequestItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_merchant")
    private Long merchantId;
    @Column(name = "id_customer")
    private Long customerId;
    @Type(type = "list-array")
    @Column(
            name = "list_item_id",
            columnDefinition = "bigint[]"
    )
    private List<Long> listItemId;
    @Type(type = "list-array")
    @Column(
            name = "list_item_amount",
            columnDefinition = "bigint[]"
    )
    private List<Long> listItemAmount;
    @Column(name = "total_price")
    private Long totalPrice;
    private Timestamp time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<Long> getListItemId() {
        return listItemId;
    }

    public void setListItemId(List<Long> listItemId) {
        this.listItemId = listItemId;
    }

    public void addItemId(Long itemId){
        this.listItemId.add(itemId);
    }

    public List<Long> getListItemAmount() {
        return listItemAmount;
    }

    public void setListItemAmount(List<Long> listItemAmount) {
        this.listItemAmount = listItemAmount;
    }

    public void addItemAmount(Long itemAmount){
        this.listItemAmount.add(itemAmount);
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void addTotalPrice(Long totalPrice){
        this.totalPrice += totalPrice;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
