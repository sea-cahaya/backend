package com.sea.cahayabe.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "merchant_details")
public class MerchantDetails {

    @Id
    @Column(name = "id_user")
    private Long id;
    @Column(name = "merchant_name")
    private String merchantName;
    private String address;
    private String phone;
    private Long wallet;

    public MerchantDetails(){

    }

    public MerchantDetails(Long id, String merchantName) {
        this.id = id;
        this.merchantName = merchantName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getWallet() {
        return wallet;
    }

    public void setWallet(Long wallet) {
        this.wallet = wallet;
    }
}
