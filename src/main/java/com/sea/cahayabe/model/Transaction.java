package com.sea.cahayabe.model;

import com.sea.cahayabe.payload.request.BuyOneItemRequest;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity(name = "transaction")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_customer")
    private Long customerId;
    private String status;
    @Column(name = "total_price")
    private Long totalPrice;
    @Column(name = "bank_name")
    private String bankName;
    @Column(name = "bank_account_number")
    private Long bankAccountNumber;
    @Type(type = "list-array")
    @Column(
            name = "list_item_id",
            columnDefinition = "bigint[]"
    )
    private List<Long> listItemId;
    @Type(type = "list-array")
    @Column(
            name = "list_item_amount",
            columnDefinition = "bigint[]"
    )
    private List<Long> listItemAmount;
    @Column(name = "transaction_time")
    private Timestamp transactionTime;

    public Transaction(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(Long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public List<Long> getListItemId() {
        return listItemId;
    }

    public void setListItemId(List<Long> listItemId) {
        this.listItemId = listItemId;
    }

    public List<Long> getListItemAmount() {
        return listItemAmount;
    }

    public void setListItemAmount(List<Long> listItemAmount) {
        this.listItemAmount = listItemAmount;
    }

    public Timestamp getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Timestamp transactionDate) {
        this.transactionTime = transactionDate;
    }

    public static Transaction buildFromItemRequest(BuyOneItemRequest itemRequest, User user, Item item){
        Transaction transaction = new Transaction();
        transaction.setCustomerId(user.getId());
        transaction.setStatus("wait");
        transaction.setTotalPrice(item.getPrice() * itemRequest.getItemAmount());
        transaction.setBankName(itemRequest.getBankName());
        transaction.setBankAccountNumber(itemRequest.getBankAccountNumber());
        transaction.setListItemId(List.of(itemRequest.getItemId()));
        transaction.setListItemAmount(List.of(itemRequest.getItemAmount()));
        transaction.setTransactionTime(new Timestamp(System.currentTimeMillis()));
        return transaction;
    }
}
