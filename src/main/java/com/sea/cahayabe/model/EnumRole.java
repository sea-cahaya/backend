package com.sea.cahayabe.model;

public enum  EnumRole {
    ROLE_CUSTOMER,
    ROLE_MERCHANT,
    ROLE_ADMIN
}
