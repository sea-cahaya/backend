package com.sea.cahayabe.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "wallet_history")
public class WalletHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "id_merchant")
    private Long merchantId;
    @Column(name= "nominal_out")
    private Long nominalOut;
    @Column(name= "nominal_in")
    private Long nominalIn;
    @Column(name = "bank_name")
    private String bankName;
    @Column(name = "bank_account_number")
    private Long bankAccountNumber;
    private Timestamp time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getNominalOut() {
        return nominalOut;
    }

    public void setNominalOut(Long nominalOut) {
        this.nominalOut = nominalOut;
    }

    public Long getNominalIn() {
        return nominalIn;
    }

    public void setNominalIn(Long nominalIn) {
        this.nominalIn = nominalIn;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(Long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
