package com.sea.cahayabe.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SampleController {

    @GetMapping("/api/test/customer")
    @PreAuthorize("hasRole('CUSTOMER')")
    public String customer(Principal principal){
        System.out.println(principal.getName());
//        System.out.println(principal.);
        System.out.println("cek cek cek");
        System.out.println("cucucucuc");
        return "Hello im customer";
    }

    @GetMapping("/")
    public String root(){
        return "Hello Cahaya";
    }
}
