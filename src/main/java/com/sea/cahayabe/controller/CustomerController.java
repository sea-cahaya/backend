package com.sea.cahayabe.controller;

import com.sea.cahayabe.model.*;
import com.sea.cahayabe.payload.dto.CustomerProfile;
import com.sea.cahayabe.payload.request.BankAccountRequest;
import com.sea.cahayabe.payload.request.BuyOneItemRequest;
import com.sea.cahayabe.payload.response.AdminTransactionResponse;
import com.sea.cahayabe.payload.response.CartResponse;
import com.sea.cahayabe.payload.response.CustomerTransactionResponse;
import com.sea.cahayabe.payload.response.MessageResponse;
import com.sea.cahayabe.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/customer")
public class CustomerController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerDetailsRepository customerDetailsRepository;
    @Autowired
    CartRepository cartRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    TransactionRepository transactionRepository;

    @GetMapping("/profile")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> getCustomerProfile(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        CustomerDetails customerDetails = customerDetailsRepository.findById(user.getId()).get();
        return ResponseEntity.ok(new CustomerProfile(user, customerDetails));
    }

    @PostMapping("/profile")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> updateCustomerProfile(Principal principal, @Valid @RequestBody CustomerProfile customerProfile){
        if (customerProfile.getUsername() == null){
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Invalid Username"));
        }
        User user = userRepository.findByUsername(principal.getName()).get();
        CustomerDetails customerDetails = customerDetailsRepository.findById(user.getId()).get();
        user.setUsername(customerProfile.getUsername());
        customerDetails.setAddress(customerProfile.getAddress());
        customerDetails.setName(customerProfile.getName());
        customerDetails.setPhone(customerProfile.getPhone());
        userRepository.save(user);
        customerDetailsRepository.save(customerDetails);
        return ResponseEntity.ok(new MessageResponse("Edit success."));
    }

    @GetMapping("/carts")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> getMyCart(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        List<Cart> carts = cartRepository.findAllByUserId(user.getId());
        List<CartResponse> cartResponses = new ArrayList<>();
        for (Cart c : carts){
            Item item = itemRepository.findById(c.getItemId()).get();
            cartResponses.add(new CartResponse(item, c));
        }
        return ResponseEntity.ok(cartResponses);
    }

    @PostMapping("/add-cart")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> addCart(Principal principal, @RequestParam Long itemId, @RequestParam Long itemAmount){
        User user = userRepository.findByUsername(principal.getName()).get();
        Item item = itemRepository.findById(itemId).get();
        if (itemAmount > item.getStock()){
            return ResponseEntity.ok(new MessageResponse(item.getStock() + " item left."));
        }
        Cart cart = new Cart(user.getId(), itemId, itemAmount);
        cartRepository.save(cart);
        return ResponseEntity.ok(new MessageResponse("Add item to cart success"));
    }

    @PostMapping("/edit-cart")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> editCart(Principal principal, @RequestParam Long cartId, @RequestParam Long cartAmount){
        Cart cart = cartRepository.findById(cartId).get();
        Item item = itemRepository.findById(cart.getItemId()).get();
        if (cartAmount > item.getStock()){
            return ResponseEntity.ok(new MessageResponse("Item out of stocks."));
        }
        cart.setAmount(cartAmount);
        cartRepository.save(cart);
        return ResponseEntity.ok(new MessageResponse("Edit item to cart success"));
    }

    @PostMapping("/delete-cart")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> deleteCart(Principal principal, @RequestParam Long cartId){
        Cart cart = cartRepository.findById(cartId).get();
        cartRepository.delete(cart);
        return ResponseEntity.ok(new MessageResponse("Delete item from cart success"));
    }

    @PostMapping("/buy-item")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> buyOneItem(Principal principal, @RequestBody BuyOneItemRequest itemRequest){
        Item item = itemRepository.findById(itemRequest.getItemId()).get();
        if (itemRequest.getItemAmount() > item.getStock()){
            return ResponseEntity.badRequest()
                    .body(new MessageResponse(String.valueOf(item.getStock()) + " items left"));
        }
        User user = userRepository.findByUsername(principal.getName()).get();
        Transaction transaction = Transaction.buildFromItemRequest(itemRequest, user, item);
        item.setStock(item.getStock() - itemRequest.getItemAmount());
        itemRepository.save(item);
        transactionRepository.save(transaction);
        return ResponseEntity.ok(new MessageResponse("Transaction success, wait for admin confirmation"));
    }

    @PostMapping("/buy-from-cart")
    @PreAuthorize("hasRole('CUSTOMER')")
    @Transactional
    public ResponseEntity<?> buyFromCustomerCart(Principal principal, @RequestBody BankAccountRequest bank){
        User user = userRepository.findByUsername(principal.getName()).get();
        List<Cart> carts = cartRepository.findAllByUserId(user.getId());
        if (carts.isEmpty()){ return ResponseEntity.ok(new MessageResponse("Empty Cart")); }
        String errMsg = "";
        boolean outOfStocks = false;
        for (Cart c : carts){
            Item temp = itemRepository.findById(c.getItemId()).get();
            if (c.getAmount() > temp.getStock()){
                errMsg = errMsg + "Item " + temp.getName() + " has " + temp.getStock() + " stocks left.\n";
                outOfStocks = true;
            }
        }
        if (outOfStocks){
            return ResponseEntity.badRequest().body(new MessageResponse(errMsg));
        }
        transactionRepository.save(getTransactionFromCart(user, carts, bank));
        //kosongkan cart
        cartRepository.deleteAllByUserId(user.getId());
        return ResponseEntity.ok(new MessageResponse("Transaction success, wait for admin confirmation"));
    }

    private Transaction getTransactionFromCart(User user, List<Cart> carts, BankAccountRequest bank){
        Transaction transaction = new Transaction();
        transaction.setCustomerId(user.getId());
        transaction.setStatus("wait");
        transaction.setBankName(bank.getBankName());
        transaction.setBankAccountNumber(bank.getBankAccountNumber());
        transaction.setTransactionTime(new Timestamp(System.currentTimeMillis()));
        Long total_price = 0L;
        List<Long> itemId = new ArrayList<>();
        List<Long> itemAmount = new ArrayList<>();
        for (Cart c : carts){
            Item temp = itemRepository.findById(c.getItemId()).get();
            total_price += c.getAmount() * temp.getPrice();
            itemId.add(c.getItemId());
            itemAmount.add(c.getAmount());
            temp.setStock(temp.getStock() - c.getAmount());
            itemRepository.save(temp);
        }
        transaction.setTotalPrice(total_price);
        transaction.setListItemId(itemId);
        transaction.setListItemAmount(itemAmount);
        return transaction;
    }

    @GetMapping("/transaction-history")
    @PreAuthorize("hasRole('CUSTOMER')")
    public ResponseEntity<?> getAllTransactionHistory(Principal principal){
        Long customerId = userRepository.findByUsername(principal.getName()).get().getId();
        CustomerTransactionResponse ctr = new CustomerTransactionResponse();
        List<Transaction> transactions = transactionRepository.findAllByCustomerId(customerId);
        if (transactions.isEmpty()){
            return ResponseEntity.ok(transactions);
        }
        List<CustomerTransactionResponse> results = transactions.stream()
                .map(t -> ctr.buildCustomerTransaction(t,itemRepository))
                .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }
}
