package com.sea.cahayabe.controller;

import com.sea.cahayabe.model.CustomerDetails;
import com.sea.cahayabe.model.EnumRole;
import com.sea.cahayabe.model.MerchantDetails;
import com.sea.cahayabe.model.User;
import com.sea.cahayabe.payload.request.AdminSignupRequest;
import com.sea.cahayabe.payload.request.CustomerSignupRequest;
import com.sea.cahayabe.payload.request.LoginRequest;
import com.sea.cahayabe.payload.request.MerchantSignupRequest;
import com.sea.cahayabe.payload.response.JwtResponse;
import com.sea.cahayabe.payload.response.MessageResponse;
import com.sea.cahayabe.repository.CustomerDetailsRepository;
import com.sea.cahayabe.repository.MerchantDetailsRepository;
import com.sea.cahayabe.repository.UserRepository;
import com.sea.cahayabe.security.jwt.JwtUtils;
import com.sea.cahayabe.security.service.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    MerchantDetailsRepository merchantDetailsRepository;

    @Autowired
    CustomerDetailsRepository customerDetailsRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    private static final String ADMIN_TOKEN = "asdfasdf";

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> role = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                role.get(0)
        ));
    }

    @PostMapping(value = "/customer-signup")
    public ResponseEntity<?> registerCustomer(@Valid @RequestBody CustomerSignupRequest customerSignupRequest){
        if (userRepository.existsByUsername(customerSignupRequest.getUsername())){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // Create new customer account
        User user = new User(customerSignupRequest.getUsername(),
                customerSignupRequest.getEmail(),
                encoder.encode(customerSignupRequest.getPassword()));
        user.setRole(EnumRole.ROLE_CUSTOMER.name());
        user.setIs_active(true);
        userRepository.save(user);
        createCustomerDetails(customerSignupRequest.getUsername());
        return ResponseEntity.ok(new MessageResponse("Customer registered successfully!"));
    }

    @PostMapping("/merchant-signup")
    public ResponseEntity<?> registerMerchant(@Valid @RequestBody MerchantSignupRequest merchantSignupRequest){
        if (userRepository.existsByUsername(merchantSignupRequest.getUsername())){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // Create new merchant account
        User user = new User(merchantSignupRequest.getUsername(),
                merchantSignupRequest.getEmail(),
                encoder.encode(merchantSignupRequest.getPassword()));
        user.setRole(EnumRole.ROLE_MERCHANT.name());
        // default merchant false, wait for admin acceptance
        user.setIs_active(false);
        userRepository.save(user);

        createMerchantDetails(merchantSignupRequest.getUsername(), merchantSignupRequest.getMerchantName());
        return ResponseEntity.ok(new MessageResponse("Merchant registered successfully!"));
    }

    @PostMapping("/admin-signup")
    public ResponseEntity<?> registerAdmin(@Valid @RequestBody AdminSignupRequest adminSignupRequest){
        if (!adminSignupRequest.getAdminToken().equals(ADMIN_TOKEN)){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Wrong Token!"));
        }

        if (userRepository.existsByUsername(adminSignupRequest.getUsername())){
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // Create new admin account
        User user = new User(adminSignupRequest.getUsername(),
                adminSignupRequest.getEmail(),
                encoder.encode(adminSignupRequest.getPassword()));
        user.setRole(EnumRole.ROLE_ADMIN.name());
        user.setIs_active(true);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("Admin registered successfully!"));
    }

    @PostMapping("/test")
    public String testPost(){
        return "hello";
    }

    private void createCustomerDetails(String username){
         User user = userRepository.findByUsername(username).get();
        customerDetailsRepository.save(new CustomerDetails(user.getId()));
    }

    private void createMerchantDetails(String username, String merchantName){
        User user = userRepository.findByUsername(username).get();
        merchantDetailsRepository.save(new MerchantDetails(user.getId(), merchantName));
    }
}
