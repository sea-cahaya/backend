package com.sea.cahayabe.controller;

import com.sea.cahayabe.model.*;
import com.sea.cahayabe.payload.response.AdminTransactionResponse;
import com.sea.cahayabe.payload.response.MessageResponse;
import com.sea.cahayabe.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/admin")
public class AdminController {
    @Autowired
    CustomerDetailsRepository customerDetailsRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    RequestItemRepository requestItemRepository;
    @Autowired
    WalletHistoryRepository walletHIstoryRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    MerchantDetailsRepository merchantDetailsRepository;

    //merchant confirmation
    @GetMapping("/merchant")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getNonActiveMerchant(){
        List<User> users = userRepository.findByRole("ROLE_MERCHANT");
        users = users.stream()
                .filter(s -> !s.getIs_active())
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    @PostMapping("/confirm-merchant")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> activateMerchant(@RequestParam String username){
        User user = userRepository.findByUsername(username).get();

        if (!user.getUsername().equals(username)){
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Username not found!"));
        }
        user.setIs_active(true);
        userRepository.save(user);
        return ResponseEntity.ok(new MessageResponse("Merchant confirmation success!"));
    }

    @GetMapping("/transactions")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getAllTransaction(){
        AdminTransactionResponse atr = new AdminTransactionResponse();
        List<AdminTransactionResponse> results = transactionRepository.findAll().stream()
                .filter(this::isTransactionStatusWait)
                .map(t -> atr.buildAdminTransaction(t, customerDetailsRepository.getCustomerNameById(t.getCustomerId()), itemRepository))
                .collect(Collectors.toList());
        return ResponseEntity.ok(results);
    }

    @PostMapping("/accept-transaction")
    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    public ResponseEntity<?> acceptTransaction(@RequestParam Long transactionId){
        System.out.println("[CHECKPOINT 1] accept transaction");
        // approve transaksi, ubah status di pengguna
        Transaction transaction = transactionRepository.findById(transactionId).get();
        if (!transaction.getStatus().equals("wait")){
            return ResponseEntity.badRequest().body(new MessageResponse("status invalid."));
        }
        transaction.setStatus("accepted");
        // masukin masing2 item ke request item merchant
        // -> iterasi item di transaction, kurangin stock, tambahin ke map request
        System.out.println("[CHECKPOINT 2] accept transaction");
        Map<Long, RequestItem> mapReqItem = new HashMap<>();
        System.out.println("[CHECKPOINT 3] accept transaction");
        for (int i = 0; i < transaction.getListItemId().size(); i++) {
            Long itemId = transaction.getListItemId().get(i);
            Long itemAmount = transaction.getListItemAmount().get(i);
            Item item = itemRepository.findById(itemId).get();
            System.out.println("[CHECKPOINT 4] accept transaction");
            if (!mapReqItem.containsKey(item.getMerchantId())){
                RequestItem requestItem = new RequestItem();
                requestItem.setMerchantId(item.getMerchantId());
                requestItem.setCustomerId(transaction.getCustomerId());
                requestItem.setListItemId(new ArrayList<>(List.of(itemId)));
                requestItem.setListItemAmount(new ArrayList<>(List.of(itemAmount)));
                requestItem.setTotalPrice(item.getPrice()*itemAmount);
                requestItem.setTime(new Timestamp(System.currentTimeMillis()));
                mapReqItem.put(item.getMerchantId(), requestItem);
            } else {
                RequestItem requestItem = mapReqItem.get(item.getMerchantId());
                requestItem.addItemId(itemId);
                requestItem.addItemAmount(itemAmount);
                requestItem.addTotalPrice(item.getPrice()*itemAmount);
            }
            System.out.println("[CHECKPOINT 5] accept transaction");
        }
        //  masukin ke history transaksi wallet OK
        // update wallet merchant OK
        //  -> iterasi value map tambahin ke histori, save ke reqItem, sekalian update wallet
        for (RequestItem ri : mapReqItem.values()){
            System.out.println("[CHECKPOINT 6] accept transaction");
            requestItemRepository.save(ri);
            System.out.println("[CHECKPOINT 7] accept transaction");
            MerchantDetails md = merchantDetailsRepository.findById(ri.getMerchantId()).get();
            if (md.getWallet() == null){
                md.setWallet(ri.getTotalPrice());
            } else {
                md.setWallet(md.getWallet() + ri.getTotalPrice());
            }
            System.out.println("[CHECKPOINT 8] accept transaction");
            merchantDetailsRepository.save(md);
            System.out.println("[CHECKPOINT 9] accept transaction");
            //add history
            WalletHistory wh = new WalletHistory();
            wh.setBankName(transaction.getBankName());
            wh.setBankAccountNumber(transaction.getBankAccountNumber());
            wh.setMerchantId(ri.getMerchantId());
            wh.setNominalIn(ri.getTotalPrice());
            wh.setNominalOut(0L);
            wh.setTime(new Timestamp(System.currentTimeMillis()));
            walletHIstoryRepository.save(wh);
            System.out.println("[CHECKPOINT 10] accept transaction");
        }
        transactionRepository.save(transaction);
        return ResponseEntity.ok(new MessageResponse("Transaction accepted."));
    }

    @PostMapping("/reject-transaction")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> rejectTransaction(@RequestParam Long transactionId){
        Transaction transaction = transactionRepository.findById(transactionId).get();
        transaction.setStatus("fail");
        List<Long> itemId = transaction.getListItemId();
        List<Long> itemAmount = transaction.getListItemAmount();
        for (int i = 0; i < itemId.size(); i++) {
            Item item = itemRepository.findById(itemId.get(i)).get();
            item.setStock(item.getStock() + itemAmount.get(i));
            itemRepository.save(item);
        }
        transactionRepository.save(transaction);
        return ResponseEntity.ok(new MessageResponse("Transaction set to fail."));
    }

    private Boolean isTransactionStatusWait(Transaction t){
        return t.getStatus().equalsIgnoreCase("wait");
    }
}
