package com.sea.cahayabe.controller;

import com.sea.cahayabe.model.Item;
import com.sea.cahayabe.payload.response.ItemDetailsResponse;
import com.sea.cahayabe.repository.ItemRepository;
import com.sea.cahayabe.repository.MerchantDetailsRepository;
import com.sea.cahayabe.repository.RequestItemRepository;
import com.sea.cahayabe.repository.WalletHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/home")
public class HomeController {
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    MerchantDetailsRepository merchantDetailsRepository;

    @GetMapping("/items")
    public ResponseEntity<?> getAllItems(){
        List<Item> items= itemRepository.findAll()
                .stream()
                .filter(Item::getActive)
                .collect(Collectors.toList());
        List<ItemDetailsResponse> res = new ArrayList<>();
        for (Item item : items){
            ItemDetailsResponse temp = new ItemDetailsResponse(merchantDetailsRepository.findById(item.getMerchantId()).get().getMerchantName(), item);
//            temp = (ItemDetailsResponse) item;
//            temp.setMerchantName(merchantDetailsRepository.findById(item.getMerchantId()).get().getMerchantName());
            res.add(temp);
        }
        return ResponseEntity.ok(res);
    }
}
