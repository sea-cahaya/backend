package com.sea.cahayabe.controller;

import com.sea.cahayabe.model.*;
import com.sea.cahayabe.payload.dto.MerchantProfile;
import com.sea.cahayabe.payload.request.AddEditItemRequest;
import com.sea.cahayabe.payload.request.TransferRequest;
import com.sea.cahayabe.payload.response.ItemResponse;
import com.sea.cahayabe.payload.response.MessageResponse;
import com.sea.cahayabe.payload.response.RequestItemResponse;
import com.sea.cahayabe.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/merchant")
public class MerchantController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    MerchantDetailsRepository merchantDetailsRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    WalletHistoryRepository walletHIstoryRepository;
    @Autowired
    RequestItemRepository requestItemRepository;
    @Autowired
    CustomerDetailsRepository customerDetailsRepository;

    @GetMapping("/profile")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getMerchantProfile(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        MerchantDetails merchantDetails = merchantDetailsRepository.findById(user.getId()).get();
        return ResponseEntity.ok(new MerchantProfile(user, merchantDetails));
    }

    @PostMapping("/profile")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> updateMerchantProfile(Principal principal, @Valid @RequestBody MerchantProfile merchantProfile){
        if (merchantProfile.getUsername() == null){
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Invalid username"));
        }
        User user = userRepository.findByUsername(principal.getName()).get();
        MerchantDetails merchantDetails = merchantDetailsRepository.findById(user.getId()).get();
        user.setUsername(merchantProfile.getUsername());
        merchantDetails.setAddress(merchantProfile.getAddress());
        merchantDetails.setMerchantName(merchantProfile.getMerchantName());
        merchantDetails.setPhone(merchantProfile.getPhone());
        userRepository.save(user);
        merchantDetailsRepository.save(merchantDetails);
        return ResponseEntity.ok(new MessageResponse("Edit success."));
    }

    @GetMapping("/items")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getAllMyItem(Principal principal){
        Long merchantId = userRepository.findByUsername(principal.getName()).get().getId();
        return ResponseEntity.ok(itemRepository.findAllByMerchantId(merchantId)
                .stream()
                .filter(Item::getActive)
                .collect(Collectors.toList()));
    }

    @GetMapping("/items/{merchantId}")
    public ResponseEntity<?> getAllItemById(@PathVariable Long merchantId){
        return ResponseEntity.ok(itemRepository.findAllByMerchantId(merchantId)
                .stream()
                .filter(Item::getActive)
                .collect(Collectors.toList()));
    }

    @GetMapping("/other-items")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getAllOthersItem(Principal principal){
        List<Item> item = itemRepository.findAll();
        Long merchantId = userRepository.findByUsername(principal.getName()).get().getId();

        item = item.stream()
                .filter(s -> !s.getMerchantId().equals(merchantId))
                .filter(Item::getActive)
                .collect(Collectors.toList());
        return ResponseEntity.ok(item);
    }

    @PostMapping("/add-item")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> addItem(Principal principal, @Valid @RequestBody AddEditItemRequest itemRequest){
        User user = userRepository.findByUsername(principal.getName()).get();
        Item item = AddEditItemRequest.buildItem(itemRequest, user.getId());
        itemRepository.save(item);
        return ResponseEntity.ok(new MessageResponse("Add item success."));
    }

    @GetMapping("/item/{id}")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getOneItem(@PathVariable Long id){
        return ResponseEntity.ok(itemRepository.findById(id));
    }

    @PostMapping("/item/{id}")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> editItem(@PathVariable Long id, @Valid @RequestBody AddEditItemRequest itemRequest){
        Item item = itemRepository.findById(id).get();
        item.setName(itemRequest.getName());
        item.setDescription(itemRequest.getDescription());
        item.setStock(itemRequest.getStock());
        item.setPrice(itemRequest.getPrice());
        itemRepository.save(item);
        return ResponseEntity.ok(new MessageResponse("Item edited."));
    }

    @PostMapping("/item/delete/{id}")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> deleteItem(@PathVariable Long id){
        Item item = itemRepository.findById(id).get();
        item.setActive(false);
        itemRepository.save(item);
        return ResponseEntity.ok(new MessageResponse("Item deleted."));
    }

    @GetMapping("/wallet/balance")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getWalletBalance(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        MerchantDetails merchantDetails = merchantDetailsRepository.findById(user.getId()).get();
        return ResponseEntity.ok(merchantDetails.getWallet());
    }

    @GetMapping("/wallet/history")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getWalletHistory(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        List<WalletHistory> wh = walletHIstoryRepository.findAllByMerchantId(user.getId());
        return ResponseEntity.ok(wh);
    }

    @PostMapping("/wallet/transfer")
    @PreAuthorize("hasRole('MERCHANT')")
    @Transactional
    public ResponseEntity<?> transferWalletBalance(Principal principal
            , @RequestBody TransferRequest tf){
        User user = userRepository.findByUsername(principal.getName()).get();
        MerchantDetails md = merchantDetailsRepository.findById(user.getId()).get();
        if (tf.getBalance() > md.getWallet()){
            return ResponseEntity.badRequest().body(new MessageResponse("The balance is not sufficient."));
        }
        md.setWallet(md.getWallet() - tf.getBalance());
        merchantDetailsRepository.save(md);
        //make history transaction
        WalletHistory wh = new WalletHistory();
        wh.setMerchantId(user.getId());
        wh.setNominalIn(0L);
        wh.setNominalOut(tf.getBalance());
        wh.setBankName(tf.getBankName());
        wh.setBankAccountNumber(tf.getBankAccountNumber());
        wh.setTime(new Timestamp(System.currentTimeMillis()));
        walletHIstoryRepository.save(wh);
        return ResponseEntity.ok(new MessageResponse("Transfer Success."));
    }

    @GetMapping("/request-item")
    @PreAuthorize("hasRole('MERCHANT')")
    public ResponseEntity<?> getRequestItem(Principal principal){
        User user = userRepository.findByUsername(principal.getName()).get();
        List<RequestItem> ri = requestItemRepository.findAllByMerchantId(user.getId());
        //mapping ke masing2
        List<RequestItemResponse> response = new ArrayList<>();
        if (ri.isEmpty()){
            return ResponseEntity.ok(response);
        }
        for (RequestItem reqItem : ri){
            User cust = userRepository.findById(reqItem.getCustomerId()).get();
            CustomerDetails custDetails = customerDetailsRepository.findById(cust.getId()).get();
            RequestItemResponse rir = new RequestItemResponse();
            rir.setCustomerName(custDetails.getName());
            rir.setCustomerAddress(custDetails.getAddress());
            rir.setCustomerPhone(custDetails.getPhone());
            rir.setTotalPrice(reqItem.getTotalPrice());
            rir.setTime(reqItem.getTime());
            List<Long> itemIds = reqItem.getListItemId();
            List<Long> itemAmounts = reqItem.getListItemAmount();
            List<ItemResponse> iResponse = new ArrayList<>();
            for (int i = 0; i < itemIds.size(); i++) {
                Item item = itemRepository.findById(itemIds.get(i)).get();
                ItemResponse ir = new ItemResponse();
                ir.setAmount(itemAmounts.get(i));
                ir.setItemName(item.getName());
                ir.setPrice(item.getPrice());
                iResponse.add(ir);
            }
            rir.setResponses(iResponse);
            response.add(rir);
        }
        return ResponseEntity.ok(response);
    }
}
