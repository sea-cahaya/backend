package com.sea.cahayabe.payload.response;

import com.sea.cahayabe.model.Item;
import com.sea.cahayabe.model.Transaction;
import com.sea.cahayabe.repository.ItemRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CustomerTransactionResponse {

    private Long id;
    private Long totalPrice;
    private String bankName;
    private Long bankAccountNumber;
    private Timestamp transactionTime;
    private String status;

    private List<ItemResponse> items;

    public CustomerTransactionResponse(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(Long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Timestamp getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Timestamp transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public List<ItemResponse> getItems() {
        return items;
    }

    public void setItems(List<ItemResponse> items) {
        this.items = items;
    }

    public CustomerTransactionResponse buildCustomerTransaction(Transaction transaction, ItemRepository itemRepository){
        CustomerTransactionResponse customerTransactionResponse = new CustomerTransactionResponse();
        customerTransactionResponse.setId(transaction.getId());
        customerTransactionResponse.setTotalPrice(transaction.getTotalPrice());
        customerTransactionResponse.setBankName(transaction.getBankName());
        customerTransactionResponse.setBankAccountNumber(transaction.getBankAccountNumber());
        customerTransactionResponse.setTransactionTime(transaction.getTransactionTime());
        customerTransactionResponse.setStatus(transaction.getStatus());
        List<Long> itemId = transaction.getListItemId();
        List<Long> amountId = transaction.getListItemAmount();
        List<ItemResponse> ir = new ArrayList<>();
        for (int i = 0; i < itemId.size(); i++) {
            ItemResponse temp = new ItemResponse();
            Item item = itemRepository.findById(itemId.get(i)).get();
            temp.setItemName(item.getName());
            temp.setPrice(item.getPrice());
            temp.setAmount(amountId.get(i));
            ir.add(temp);
        }
        customerTransactionResponse.setItems(ir);
        return customerTransactionResponse;
    }

}
