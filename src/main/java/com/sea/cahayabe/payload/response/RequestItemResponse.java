package com.sea.cahayabe.payload.response;

import com.sea.cahayabe.model.RequestItem;

import java.sql.Timestamp;
import java.util.List;

public class RequestItemResponse {
    private String customerName;
    private String customerAddress;
    private String customerPhone;
    private Long totalPrice;
    private List<ItemResponse> responses;
    private Timestamp time;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<ItemResponse> getResponses() {
        return responses;
    }

    public void setResponses(List<ItemResponse> responses) {
        this.responses = responses;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
