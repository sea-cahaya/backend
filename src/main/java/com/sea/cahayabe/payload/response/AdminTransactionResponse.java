package com.sea.cahayabe.payload.response;

import com.sea.cahayabe.model.CustomerDetails;
import com.sea.cahayabe.model.Item;
import com.sea.cahayabe.model.Transaction;
import com.sea.cahayabe.repository.CustomerDetailsRepository;
import com.sea.cahayabe.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AdminTransactionResponse {

    private Long id;
    private String name;
    private Long totalPrice;
    private String bankName;
    private Long bankAccountNumber;
    private Timestamp transactionTime;
    private List<ItemResponse> items;

    public AdminTransactionResponse(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(Long bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public Timestamp getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Timestamp transactionTime) {
        this.transactionTime = transactionTime;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public void setItems(List<ItemResponse> items) {
        this.items = items;
    }

    public AdminTransactionResponse buildAdminTransaction(Transaction transaction, String customerName, ItemRepository itemRepository){
        AdminTransactionResponse adminTransactionResponse = new AdminTransactionResponse();
        adminTransactionResponse.setId(transaction.getId());
        adminTransactionResponse.setTotalPrice(transaction.getTotalPrice());
        adminTransactionResponse.setBankName(transaction.getBankName());
        adminTransactionResponse.setBankAccountNumber(transaction.getBankAccountNumber());
        adminTransactionResponse.setTransactionTime(transaction.getTransactionTime());
        adminTransactionResponse.setName(customerName);
        List<Long> itemId = transaction.getListItemId();
        List<Long> amountId = transaction.getListItemAmount();
        List<ItemResponse> ir = new ArrayList<>();
        for (int i = 0; i < itemId.size(); i++) {
            ItemResponse temp = new ItemResponse();
            Item item = itemRepository.findById(itemId.get(i)).get();
            temp.setItemName(item.getName());
            temp.setPrice(item.getPrice());
            temp.setAmount(amountId.get(i));
            ir.add(temp);
        }
        adminTransactionResponse.setItems(ir);
        return adminTransactionResponse;
    }

}
