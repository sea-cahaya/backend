package com.sea.cahayabe.payload.response;

import com.sea.cahayabe.model.Cart;
import com.sea.cahayabe.model.Item;

public class CartResponse {
    private Long cartId;
    private String itemName;
    private Long price;
    private Long amount;

    public CartResponse(){}

    public CartResponse(Long cartId, String itemName, Long price, Long amount) {
        this.cartId = cartId;
        this.itemName = itemName;
        this.price = price;
        this.amount = amount;
    }

    public CartResponse(Item item, Cart cart){
        this.cartId = cart.getId();
        this.itemName = item.getName();
        this.price = item.getPrice();
        this.amount = cart.getAmount();
    }

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
