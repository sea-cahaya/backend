package com.sea.cahayabe.payload.response;

import com.sea.cahayabe.model.Item;

public class ItemDetailsResponse extends Item{
    private String merchantName;

    public ItemDetailsResponse(String merchantName, Item item) {
        this.merchantName = merchantName;
        this.setDescription(item.getDescription());
        this.setId(item.getId());
        this.setName(item.getName());
        this.setPrice(item.getPrice());
        this.setStock(item.getStock());
        this.setMerchantId(item.getMerchantId());
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
}
