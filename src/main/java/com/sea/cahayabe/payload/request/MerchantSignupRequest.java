package com.sea.cahayabe.payload.request;

public class MerchantSignupRequest extends CustomerSignupRequest{
    private String merchantName;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    @Override
    public String toString() {
        return "MerchantSignupRequest{" +
                "namaToko='" + merchantName + '\'' +
                '}' + super.toString();
    }
}
