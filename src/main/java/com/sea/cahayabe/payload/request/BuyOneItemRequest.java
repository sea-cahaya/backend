package com.sea.cahayabe.payload.request;

public class BuyOneItemRequest extends  BankAccountRequest{
    private Long itemId;
    private Long itemAmount;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Long itemAmount) {
        this.itemAmount = itemAmount;
    }

}
