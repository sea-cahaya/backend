package com.sea.cahayabe.payload.request;

public class TransferRequest extends BankAccountRequest {
    private Long balance;

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }
}
