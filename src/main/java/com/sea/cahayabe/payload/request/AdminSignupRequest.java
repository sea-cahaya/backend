package com.sea.cahayabe.payload.request;

public class AdminSignupRequest extends CustomerSignupRequest{
    private String adminToken;

    public String getAdminToken() {
        return adminToken;
    }

    public void setAdminToken(String adminToken) {
        this.adminToken = adminToken;
    }
}
