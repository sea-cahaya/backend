package com.sea.cahayabe.payload.request;

import com.sea.cahayabe.model.Item;

public class AddEditItemRequest {
    private String name;
    private String description;
    private Long price;
    private Long stock;

    public AddEditItemRequest(){}

    public AddEditItemRequest(String name, String description, Long price, Long stock) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.stock = stock;
    }

    public static Item buildItem(AddEditItemRequest itemRequest, Long merchantId){
        Item item = new Item();
        item.setMerchantId(merchantId);
        item.setName(itemRequest.getName());
        item.setDescription(itemRequest.getDescription());
        item.setPrice(itemRequest.getPrice());
        item.setStock(itemRequest.getStock());
        item.setActive(true);
        return item;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
