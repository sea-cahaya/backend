package com.sea.cahayabe.payload.dto;

import com.sea.cahayabe.model.MerchantDetails;
import com.sea.cahayabe.model.User;

public class MerchantProfile {
    private String username;
    private String merchantName;
    private String address;
    private String phone;

    public MerchantProfile(){

    }

    public MerchantProfile(String username, String merchantName, String address, String phone) {
        this.username = username;
        this.merchantName = merchantName;
        this.address = address;
        this.phone = phone;
    }

    public MerchantProfile(User user, MerchantDetails merchantDetails){
        this.username = user.getUsername();
        this.merchantName = merchantDetails.getMerchantName();
        this.address = merchantDetails.getAddress();
        this.phone = merchantDetails.getPhone();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "MerchantProfile{" +
                "username='" + username + '\'' +
                ", merchantName='" + merchantName + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
