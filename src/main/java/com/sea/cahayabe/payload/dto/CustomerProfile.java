package com.sea.cahayabe.payload.dto;

import com.sea.cahayabe.model.CustomerDetails;
import com.sea.cahayabe.model.User;

public class CustomerProfile {
    private String username;
    private String name;
    private String address;
    private String phone;

    public CustomerProfile(){

    }

    public CustomerProfile(String username, String name, String address, String phone) {
        this.username = username;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public CustomerProfile(User user, CustomerDetails customerDetails){
        this.username = user.getUsername();
        this.name = customerDetails.getName();
        this.address = customerDetails.getAddress();
        this.phone = customerDetails.getPhone();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
