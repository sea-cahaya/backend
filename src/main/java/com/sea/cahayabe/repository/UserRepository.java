package com.sea.cahayabe.repository;

import com.sea.cahayabe.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    List<User> findByRole(String role);
    Boolean existsByUsername(String username);
//    Boolean existByEmail(String email);
}
