package com.sea.cahayabe.repository;

import com.sea.cahayabe.model.RequestItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RequestItemRepository extends JpaRepository<RequestItem, Long> {
    List<RequestItem> findAllByMerchantId(Long merchantId);
}
