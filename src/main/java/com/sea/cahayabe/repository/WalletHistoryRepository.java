package com.sea.cahayabe.repository;

import com.sea.cahayabe.model.WalletHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WalletHistoryRepository extends JpaRepository<WalletHistory, Long> {
    List<WalletHistory> findAllByMerchantId(Long merchantId);
}
