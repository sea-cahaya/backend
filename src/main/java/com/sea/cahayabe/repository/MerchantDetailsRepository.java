package com.sea.cahayabe.repository;

import com.sea.cahayabe.model.MerchantDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantDetailsRepository extends JpaRepository<MerchantDetails, Long> {
}
