package com.sea.cahayabe.repository;

import com.sea.cahayabe.model.CustomerDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerDetailsRepository extends JpaRepository<CustomerDetails, Long> {
    @Query(value = "select name from customer_details where customer_details.id_user = :id", nativeQuery = true)
    String getCustomerNameById(Long id);
}
