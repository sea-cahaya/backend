package com.sea.cahayabe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableSwagger2
public class CahayaBeApplication {
	public static void main(String[] args) {
		SpringApplication.run(CahayaBeApplication.class, args);
	}

}
