# Cahaya Backend
Api Documentation : [Cahaya Backend Documentation](http://be-cahaya.herokuapp.com/swagger-ui.html)

## Table of Contents
- [Table of Contents](#table-of-contents)
- [Dependencies](#dependencies)
- [Maven Scripts](#maven-scripts)
  - [`mvn spring-boot:run`](#mvn-spring-bootrun)
  - [`mvn clean package`](#mvn-clean-package)
  - [`mvn clean test`](#mvn-test)
  
## Dependencies
1. Java 11
2. Spring boot
3. maven
4. Postgre SQL

(Optional)
5. Docker

## Installation

You need Java version 11 to run this project. 
You can also run this project on development using Docker.

### maven

```
# install project's dependecies
mvn install

# run on development environment
# listening on http://localhost:8080
mvn spring-boot:run
```

### docker

```
# run on development environment
# listening on http://localhost:8080
docker-compose up

# remove containers
docker-compose down

# remove containers and database volume
docker-compose down -v
```

## Maven Scripts

In the project directory, you can run:

### `mvn spring-boot:run`

Runs the app.<br />
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `mvn clean package`
Run this script to create .jar file.

### `mvn test`
Run this script for testing.
