create table users (
    id serial primary key,
    username varchar(255) not null unique,
    email varchar(255) not null,
    password varchar(255) not null,
    role varchar(255) not null,
    is_active boolean default false
);

create table customer_details (
    id_user bigint primary key,
    name varchar(255),
    address text,
    phone varchar(255),
    foreign key(id_user) references users(id)
);

create table merchant_details (
    id_user bigint primary key,
    merchant_name varchar(255),
    address text,
    phone varchar(255),
    wallet bigint default 0,
    foreign key(id_user) references users(id)
);

create table items (
    id serial primary key,
    id_merchant bigint,
    name varchar(255) not null,
    description text,
    price bigint not null,
    stock bigint not null,
    is_active boolean default true,
    foreign key(id_merchant) references users(id)
);

create table cart (
    id serial primary key,
    id_user bigint,
    id_item bigint,
    amount bigint not null,
    foreign key(id_user) references users(id),
    foreign key(id_item) references items(id) on delete cascade
);

create table transaction (
    id serial primary key,
    id_customer bigint,
    status varchar(10),
    total_price bigint,
    bank_name varchar(255),
    bank_account_number bigint,
    list_item_id bigint[] default '{}',
    list_item_amount bigint[] default '{}',
    transaction_time TIMESTAMP,
    foreign key(id_customer) references users(id)
);

create table request_item (
    id serial primary key,
    id_merchant bigint,
    id_customer bigint,
    list_item_id bigint[] default '{}',
    list_item_amount bigint[] default '{}',
    total_price bigint,
    time TIMESTAMP,
    foreign key(id_merchant) references users(id),
    foreign key(id_customer) references users(id)
);

create table wallet_history (
    id serial primary key,
    id_merchant bigint,
    nominal_out bigint,
    nominal_in bigint,
    bank_name varchar(255),
    bank_account_number bigint,
    time TIMESTAMP,
    foreign key(id_merchant) references users(id)
);
